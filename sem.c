#include<stdio.h>

int wait(int s){
  return (--s);
}

int signal(int s){
  return (++s);
}

int mut=1, ful=0, emt,x=0;

void producer() {
  mut=wait(mut);
  ful=signal(ful);
  emt=wait(emt);
  x++;
  printf("\nProduced %d", x);
  mut=signal(mut);
}

void consumer(){
  mut=wait(mut);
  ful=wait(ful);
  emt=signal(emt);
  printf("\nConsume %d",x);
  x--;
  mut=signal(mut);
}

void main() {
  int c;
  scanf("%d", &emt);
  while(1){
    printf("\n1.Producer\n2.Consumer\n3.Exit\nChoice: ");
    scanf("%d",&c);
    switch(c){
      case 1: if((mut==1)&&(emt!=0)) producer();
              else printf("Buffer full!!"); break;
      case 2: if((mut==1)&&(ful!=0)) consumer();
              else printf("Buffer empty!!"); break;
      default: exit(0);
    }
  }
  exit(0);
}
