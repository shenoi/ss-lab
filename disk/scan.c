#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"

struct request
{
	int pos;
	bool v;
};

int main()
{
	int i,n,head,max,j;
	printf("Cylinder size: ");
	scanf("%d",&max);
	printf("Size of request queue: ");
	scanf("%d",&n);
	struct request queue[n];
    printf("Disk positions to be read\n");
	for (i = 0; i < n; ++i)
	{
		scanf("%d",&queue[i].pos);
		while(queue[i].pos < 0 || queue[i].pos > max){
    		printf("Please enter a valid position: ");
    		scanf("%d",&queue[i].pos);
    	}
		queue[i].v = false;
	}
	printf("Enter initial position of R/W head: ");
	scanf("%d",&head);

	
	int seek=0;
	printf("%d -> ",head );

	for(i=head;i>=0;i--){
		for(j=0;j<n;j++){
			if(queue[j].pos == i && queue[j].v == false){
				printf("%d -> ", queue[j].pos);
				queue[j].v = true;
				seek += abs(queue[j].pos - head);
				head = queue[j].pos;
			}
		}
	}
	printf("%d -> ", 0);
	seek += abs(0 - head);
	head = 0;
	for(i=head;i<max;i++){
		for(j=0;j<n;j++){
			if(queue[j].pos == i && queue[j].v == false){
				printf("%d -> ", queue[j].pos);
				queue[j].v = true;
				seek += abs(queue[j].pos - head);
				head = queue[j].pos;
			}
		}
	}
	seek += abs(max-1 - head );
	printf("%d \n", max-1);
		
	float avg = seek/(float)n;
	printf("Seek Time: %d Average: %f\n", seek, avg);
}
