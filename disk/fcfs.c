#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"


int main()
{
    int queue[20],n,head,i,j,k,seek=0,max,diff;
    float avg;
	printf("Cylinder size: ");
    scanf("%d",&max);
    printf("Size of request queue: ");
    scanf("%d",&n);
    printf("Disk positions to be read\n");
    for(i=1;i<=n;i++){
    	scanf("%d",&queue[i]);
    	while(queue[i] < 0 || queue[i] > max){
    		printf("Please enter a valid position: ");
    		scanf("%d",&queue[i]);
    	}
    }
    printf("Initial head position: ");
	scanf("%d",&head);
	queue[0]=head;
	for(j=0;j<=n-1;j++) {
		diff=abs(queue[j+1]-queue[j]);
		seek+=diff;
		printf("%d -> ",queue[j]);
    }
    printf("%d\n", queue[j]);
    printf("Seek Time: %d  ",seek);
    avg=seek/(float)n;
    printf("Average: %f\n",avg);
    return 0;
}
