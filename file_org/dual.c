#include <stdio.h>

void main()
{
	int count, s[20];
    char f[20][20][20];
    char d[20][20];
    int i, j;
    printf("Number of users: ");
    scanf("%d", &count);
    printf("\n* User *\n");
    for (i = 0; i < count; i++)
    {
        printf("%d Name: ", i + 1);
        scanf("%s", &d[i]);
        printf("%d No. of Files: ", i + 1);
        scanf("%d", &s[i]);
    }

    printf("File names\n");
    for (i = 0; i < count; i++)
        for (j = 0; j < s[i]; j++)
        {
            printf("%d/%d: ", i + 1, j + 1);
            scanf("%s", &f[i][j]);
        }
    printf("\n");
    printf("User\tSize\tFilename\n");
    for (i = 0; i < count; i++)
    {
        printf("%s\t%2d\t", d[i], s[i]);
        for (j = 0; j < s[i]; j++)
            printf("%s\n\t\t", f[i][j]);
        printf("\n");
    }
}
