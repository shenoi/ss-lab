# Q. Implement Banker's Algorithm for deadlock avoidance
m, n = int(input("n: ")), int(input("m: "))
print("No. of Processes(n): ", n, "\nNo. of  resources(m): ", m)
print("Enter Available")
Avail = [int(input("%d: " % (i+1))) for i in range(m)]
print("Enter Max")
Max = [[int(input("%d/%d: " % (i+1, j+1)))
        for j in range(m)] for i in range(n)]
print("Enter Alloc")
Alloc = [[int(input("%d/%d: " % (i+1, j+1)))
          for j in range(m)] for i in range(n)]
Need = [[Max[i][j] - Alloc[i][j] for j in range(m)] for i in range(n)]

# Display of input as a table
print("P#\tAllocation\t\tMax\t\tNeed")
for i in range(3):
    for j in range(m):
        print("\t", j + 1, sep="", end="")

for i in range(n):
    print("\n", i + 1, sep="", end="|")
    for j in range(m):
        print("\t", Alloc[i][j], sep="", end="")
    for j in range(m):
        print("\t", Max[i][j], sep="", end="")
    for j in range(m):
        print("\t", Need[i][j], sep="", end="")

print("\nAvailable:", end="")
for j in range(m):
    print("\t", Avail[j], sep="", end="")

# Safety Algorithm
Fin, Seq, s, ind = [0, 0, 0, 0, 0], [], 0, 0
for j in range(n):
    for i in range(n):
        if (Fin[i] == 0):
            f = 1
            for j in range(m):
                if (Need[i][j] > Avail[j]):
                    f = 0
                    break
            if (f == 1):
                Seq.append(i)
                for j in range(m):
                    Avail[j] += Alloc[i][j]
                Fin[i] = 1

print("\nSafe!\nThe sequence is", end="")
for i in range(n):
    print("-> P", Seq[i], sep="", end="")
print()
