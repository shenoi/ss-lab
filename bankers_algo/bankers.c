#include <stdio.h>
// Q. Implement Banker's Algorithm for deadlock avoidance
void main()
{
    int m, n, i, j;
    printf("n: ");
    scanf("%d", &n);
    printf("m: ");
    scanf("%d", &m);
    printf("No. of Processes(n): %d\n", n);
    printf("No. of  resources(m): %d\n", m);
    int Alloc[5][3], Max[5][3], Avail[3], Need[n][m];
    printf("Enter Available\n");
    for (i = 0; i < n; i++)
    {
        printf("%d: ", i + 1);
        scanf("%d", &Avail[i]);
    }
    printf("Enter Max\n");
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            printf("%d/%d: ", i + 1, j + 1);
            scanf("%d", &Max[i][j]);
        }
    }
    printf("Enter Allocation\n");
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%d/%d: ", i + 1, j + 1);
            scanf("%d", &Alloc[i][j]);
        }
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
            Need[i][j] = Max[i][j] - Alloc[i][j];
    }

    /* Display of input as a table */
    printf("P#\tAllocation\t\tMax\t\tNeed\n");
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < m; j++)
            printf("\t%d", j + 1);
    }
    for (i = 0; i < n; i++)
    {
        printf("\n%d|", i + 1);
        for (j = 0; j < m; j++)
            printf("\t%d", Alloc[i][j]);
        for (j = 0; j < m; j++)
            printf("\t%d", Max[i][j]);
        for (j = 0; j < m; j++)
            printf("\t%d", Need[i][j]);
    }
    printf("\nAvailable: ");
    for (j = 0; j < m; j++)
    {
        printf("\t%d", Avail[j]);
    }

    /* Safety Algorithm */
    int Fin[5] = {0, 0, 0, 0, 0}, Seq[5], f, in = 0;
    for (j = 0; j < n; j++)
    {
        for (i = 0; i < n; i++)
        {
            if (Fin[i] == 0)
            {
                f = 1;
                for (j = 0; j < m; j++)
                {
                    if (Need[i][j] > Avail[j])
                    {
                        f = 0;
                        break;
                    }
                }
                if (f == 1)
                {
                    Seq[in++] = i;
                    for (j = 0; j < m; j++)
                        Avail[j] += Alloc[i][j];
                    Fin[i] = 1;
                }
            }
        }
    }
    printf("\nSafe!\nThe sequence is");
    for (i = 0; i < n; i++)
        printf("-> P%d", Seq[i]);
    printf("\n");
}
