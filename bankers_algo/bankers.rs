// Implement Banker's Algorithm for deadlock avoidance
use std::io;

fn main() {
    /// M and N are the number of resources and processes in the system. Both are unisigned integers.
    let m:usize = 3;
    let n:usize = 5;
    println!("No. of Processes(n): {}\nNo. of  resources(m): {}", n, m);

    let mut inp = String::new();

    /// available is an array indicating the number of resources "available". 
    let mut avail:Vec<i32> = vec![3,3,2];

    /// alloc is a matrix indicating no. of resources allocated per process.
    let mut alloc:Vec<Vec<i32>> = vec![vec![0,1,0], vec![2,0,0], vec![3,0,2], vec![2,1,1], vec![0,0,2]];

    /// max is a matrix indicating max no. of resources that are demanded for a process to complete.
    let mut max:Vec<Vec<i32>> = vec![vec![7,5,3], vec![3,2,2], vec![9,0,2], vec![2,2,2], vec![4,3,3]];

    /// need is a matrix indicating the remaining no. of resources that are still demanded by the process.
    let mut need:Vec<Vec<i32>> = vec![];
    for i in 0..n {
        let mut temp = vec![];
        for j in 0..m {
            temp.push(max[i][j] - alloc[i][j]);
        }
        need.push(temp);
    }

    // Display of input as a table
    println!("P#\tallocation\t\tmax\t\t\tneed");

    for i in 0..n {
        print!("\n{} |", i+1);
        for j in 0..m {
            print!("\t{}", alloc[i][j]);
        }
        for j in 0..m {
            print!("\t{}", max[i][j]);
        }
        for j in 0..m {
            print!("\t{}", need[i][j]);
        }
        println!("");
    }

    print!("\navail: ");
    for j in 0..m {
        print!("\t{}", avail[j]);
    }

    // Safety Algorithm
    let mut fin = vec![0; n];
    let mut seq = vec![];
    loop {
        for i in 0..n {
        if fin[i] == 0 {
            let mut f = 1;
            for j in 0..m {
                if need[i][j] > avail[j] {
                    f = 0;
                    break;
                }
            }

            if f == 1 {
                seq.push(i);
                for j in 0..m {
                    avail[j] += alloc[i][j];
                }
                fin[i] = 1;
            }
        }
    }
    if seq.len() == n {
        break;
    }
    }

    print!("\nSafe!\nThe sequence is: ");
    for i in 0..n {
        print!("-> P{}", seq[i]);
    }
    println!("");
}